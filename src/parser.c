/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"

/*
 * Check for \r, \n, \r\n or EOF and consume it.
 */
static int
endofline(FILE *fp, int c)
{
	int eol = (c == '\r' || c == '\n');
	if (c == '\r') {
		c = getc(fp);
		if (c != '\n' || c != EOF) {
			ungetc(c, fp);
		}
	}

	return (eol);
}

/*
 * Reads one line from the input file fp assuming that lines are terminated by
 * \r, \n, \r\n or EOF.
 * On success, returns a pointer to the line with terminator removed, otherwise
 * 0 if EOF occurred.
 *
 * NOTE: reads at most LINE_MAX bytes. The caller is responsible for disposing
 * the returned pointer.
 */
char *
parser_readline(FILE *fp)
{
	char *line;
	size_t i;
	int c;

	line = calloc(LINE_MAX, 1);
	if (line == 0) {
		goto alloc_err;
	}

	for (i = 0, c = 0;
	    i < LINE_MAX - 1 && (c = getc(fp)) != EOF && !endofline(fp, c);
	    i++) {
		line[i] = (char)c;
	}

	line[i] = '\0';

	if (c == EOF && i == 0) {
		free(line);
		line = 0;
	}

alloc_err:
	return (line);
}

/* Returns a pointer to the next separator that ends the quoted field. */
static char *
advquoted(char *p, const char *sep)
{
	size_t i, j, k;

	for (i = j = 0; p[j] != '\0'; i++, j++) {
		if (p[j] == '"' && p[++j] != '"') {
			/* Copy up to next separator or \0 */
			k = strcspn(p+j, sep);
			memmove(p+i, p+j, k);
			i += k;
			j += k;
			break;
		}
		p[i] = p[j];
	}
	p[i] = '\0';
	return (p + j);
}

/*
 * Parses a line whose elements are separated by `sep`. Each element can contain
 * quotes, which can in turn contain the separator character.
 *
 * On success, the number of fields parsed is returned which are contained in
 * the `fields` parameter. On failure, either 0 or -ENOMEM is returned.
 *
 * NOTE: there's no upper bound on the number of elements that each line can
 * have.
 */
size_t
parser_parse(char *line, const char *sep, char **fields, int *status)
{
	size_t nfields;
	char *p, **newf;
	char *sepp; /* pointer to temporary separator character */
	int sepc;   /* temporary separator character */

	*status = 1;
	nfields = 0;
	if (line == 0 || line[0] == '\0') {
		*status = ENODATA;
		return (0);
	}
	p = line;

	do {
		if (nfields >= MAX_FIELDS) {
			newf = realloc(fields,
			    MAX_FIELDS * 2 * sizeof(fields[0]));
			if (newf == 0) {
				*status = ENOMEM;
				return (0);
			}
			fields = newf;
		}

		if (*p == '"') {
			sepp = advquoted(++p, sep);
		} else {
			sepp = p + strcspn(p, sep);
		}
		sepc = sepp[0];
		sepp[0] = '\0';
		fields[nfields++] = p;
		p = sepp + 1;
	} while (sepc == sep[0]);

	return (nfields);
}

