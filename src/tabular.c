/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2021 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "parser.h"

struct tbl {
	char	**list;		/* Columns */
	int	*width;		/* Width of each column */
	size_t	cols;		/* Number of columns */
};

static char **list;			/* Array of records */
static size_t entries;			/* Number of records */
static size_t maxentries = BUFSIZ;	/* Upper bound on number of records */

static void
usage(void)
{
	(void)fprintf(stderr, "usage: tabular [-s sep] [files ...]\n");
	exit(1);
}

static int
input(const char *path)
{
	char **nlist = 0;
	FILE *fp = stdin;
	char *buf;
	int ret = 1;

	if (path[0] != '-') {
		fp = fopen(path, "r");
		if (fp == 0) {
			ret = 0;
			perror("tabular");
			goto open_err;
		}
	}

	if (list == 0) {
		list = calloc(maxentries, sizeof *list);
		if (list == 0) {
			ret = 0;
			perror("tabular");
			goto alloc_err;
		}
	}

	while ((buf = parser_readline(fp)) != 0) {
		if (entries == maxentries) {
			nlist = realloc(list, (maxentries + BUFSIZ) *
			    sizeof(*nlist));

			if (nlist == 0) {
				ret = 0;
				perror("tabular");
				goto alloc_err;
			}

			(void)memset(nlist + maxentries, 0, (sizeof *nlist) *
			    BUFSIZ);
			maxentries += BUFSIZ;
			list = nlist;
		}
		list[entries++] = buf;
	}

alloc_err:
	(void)fclose(fp);
open_err:
	return (ret);
}

static int
maketbl(const char *sep)
{
	struct tbl *t, *tbl;
	char **cols, **ncols;
	char **lp;
	int *width, *nwidth;
	size_t cnt;
	size_t coloff;
	size_t maxcols;
	size_t nfields;
	int pstat;
	int ret;

	ret = 1;
	maxcols = MAX_FIELDS;
	t = tbl = calloc(entries, sizeof *t);
	cols = calloc(maxcols, sizeof *cols);
	width = calloc(maxcols, sizeof *width);
	if (tbl == 0 || cols == 0 || width == 0) {
		ret = 0;
		perror("tabular");
		goto alloc_err;
	}

	lp = list;
	for (cnt = 0, nfields = 0; cnt < entries; cnt++, lp++, t++) {
		nfields = parser_parse(list[cnt], sep, cols, &pstat);

		if (nfields > maxcols) {
			ncols = realloc(cols, (maxcols + MAX_FIELDS) *
			    (sizeof *ncols));
			nwidth = realloc(width, (maxcols + MAX_FIELDS) *
			    (sizeof *nwidth));

			if (ncols == 0 || nwidth == 0) {
				ret = 1;
				perror("tabular");
				goto alloc_err;
			}

			cols = ncols;
			width = nwidth;

			(void)memset(cols + maxcols, 0, MAX_FIELDS *
			    (sizeof *cols));
			(void)memset(width + maxcols, 0, MAX_FIELDS *
			    (sizeof *width));

			maxcols += MAX_FIELDS;
		}

		if (pstat == ENOMEM) {
			ret = 1;
			errno = pstat;
			perror("tabular");
			errno = 0;
			goto parse_err;
		}

		t->list = calloc(nfields, sizeof *(t->list));
		t->width = calloc(nfields, sizeof *(t->width));
		t->cols = nfields;
		if (t->list == 0 || t->width == 0) {
			ret = 1;
			perror("tabular");
			goto alloc_err;
		}

		for (coloff = 0; coloff < nfields; coloff++) {
			t->list[coloff] = cols[coloff];
			t->width[coloff] = (int)strlen(cols[coloff]);

			if (t->width[coloff] > width[coloff]) {
				width[coloff] = t->width[coloff];
			}
		}
	}

	for (cnt = 0, t = tbl; cnt < entries; cnt++, t++) {
		if (t != 0) {
			for (coloff = 0; coloff < t->cols; coloff++) {
				if (coloff < t->cols - 1) {
					(void)printf("%s%*s", t->list[coloff],
					    width[coloff] - t->width[coloff] +
					    2, " ");
				} else {
					(void)printf("%s\n", t->list[coloff]);
				}
			}
		}
	}

parse_err:
alloc_err:
	for (cnt = 0, t = tbl; cnt < entries; cnt++, t++) {
		free(t->list);
		free(t->width);
	}
	free(tbl);
	free(cols);
	free(width);
	return (ret);
}

static void
freelist(void)
{
	for (size_t i = 0; i < maxentries; i++) {
		free(list[i]);
		list[i] = 0;
	}
	free(list);
	list = 0;
}

int
main(int argc, char *argv[])
{
	int opt;
	char sep[] = ",";
	size_t len;

	while ((opt = getopt(argc, argv, "s:")) != -1) {
		switch (opt) {
		case 's':
			len = strlen(optarg);
			if (len > 1) {
				errno = EINVAL;
				perror("tabular");
				return (1);
			}
			(void)memcpy(sep, optarg, len);
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	char *def[] = { "-" };
	if (argc == 0) {
		argc = 1;
		argv = def;
	}

	for (int i = 0; i < argc; i++) {
		if (input(argv[i]) == 0) {
			goto cleanup;
		}
		if (maketbl(sep) == 0) {
			goto cleanup;
		}
		freelist();
	}

	return (0);
	/* UNREACHABLE*/
cleanup:
	freelist();
	return (1);
}

